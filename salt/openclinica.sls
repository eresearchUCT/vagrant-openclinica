tomcat-service:
  service.running:
    - name: tomcat7
    - enable: True

openclinica:
  file.managed: 
     - name: /var/lib/tomcat7/webapps/OpenClinica.war
     - source: salt://templates/OpenClinica.war
     - mode: 544
     - template: jinja
     - require:
        - pkg: tomcat7
