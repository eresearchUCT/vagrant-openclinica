postgresql:
  pkg.installed:
    - names:
      - postgresql
  file.managed:
    - name: /etc/postgresql/9.3/main/pg_hba.conf
    - source: salt://templates/pg_hba.conf
    - mode: 644
    - template: jinja
    - require:
        - pkg: postgresql
  service.running:
    - enable: True
    - watch: 
      - pkg: postgresql

UpdateDB:
   file.managed: 
     - name: /tmp/setup-db.sh
     - source: salt://templates/setup-db.sh
     - mode: 544
     - template: jinja
     - require:
        - pkg: postgresql
    
   cmd.run:
     - name: /tmp/setup-db.sh
