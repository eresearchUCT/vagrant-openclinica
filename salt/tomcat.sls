tomcat7:
  pkg.installed:
   - pkgs:
      - tomcat7
  service.running:
    - enable: True
    - watch:
      - pkg: tomcat7

Change Directory:
   cmd.run:
    - name: chown -R tomcat7:tomcat7 /usr/share/tomcat7
