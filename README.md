# Vagrant OpenClinica Installation #

## Description ##
OpenClinica is the world's first commercial open source clinical trial software for Electronic Data Capture (EDC) Clinical Data Management (CDM).

## Installation of Dependencies: ##
The following dependencies need to be downloaded and installed on your computer. 

* Download [Git](http://git-scm.com/downloads)
* Download [Oracle VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* Download [Vagrant ](http://www.vagrantup.com/downloads.html)

## Boot OpenClinica ##

Upon having all the dependencies installed and configured.

```
#!

git clone https://bitbucket.org/eresearchUCT/vagrant-openclinica.git
```
